package mirai

import net.mamoe.mirai.Bot
import kotlin.text.get

val groupId by config.key(0L, "广播的群号,0代表不启用")

fun broadcast(msg: String) {
    if (groupId <= 0) return
    Bot.instancesSequence.forEach {
        launch {
            val group = it.getGroup(groupId)
            group?.sendMessage(msg)
        }
    }
}

export(::broadcast)

command("send", "向此服务器交流群发送消息") {
    this.type = CommandType.Client
    usage = "<消息(忽略空格)>"
    body {
        val text = player?.name() + "：" + "\"" + arg.get(0) + "\""
        val text2 = "[yellow]" + player?.name() + " 对QQ群说：" + "\"" + arg.get(0) + "\""
        broadcast(text)
        player?.sendMessage("[royal]消息发送成功！".with())
        broadcast(text2.with())
    }
}