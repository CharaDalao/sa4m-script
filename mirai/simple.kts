package mirai

import coreMindustry.lib.broadcast
import net.mamoe.mirai.message.data.MessageSource.Key.quote
import kotlin.text.*

globalEventChannel().subscribeGroupMessages {
    contains("发送", true).reply {
        if(this.message.content.split("发送 ").get(1).count() >= 1) {
            subject.sendMessage(message.quote() + "消息发送成功！")
            broadcast("{Nickname}: \"{s}\"".with("Nickname" to sender.nick, "s" to this.message.content.split("发送 ").get(1)))
        }
        else{
            subject.sendMessage(message.quote() + "消息发送失败！请至少输入1个字！！")
        }
    }
}

